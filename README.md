## Sopel Modules

Various modules for the excellent [Sopel](http://sopel.chat) bot.

### Modules

1. ss_weather.py - A module to check the current and forecast weather, and any watches or warnings for a location.
1. ss_bible-esv.py - A module to look up bible verses via the ESV API

### Prerequisites

See the README in the module directory for module-specific prerequisites.

### Setup

See the README in the module directory for module-specific setup instructions.
