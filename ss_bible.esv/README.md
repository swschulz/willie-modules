## ss_bible-esv.py

A Willie module to look up Bible verses using the ESV API.

### Prerequisites

  * Requests - Python web library
  * re - Python regular expression library (default)

### Setup

1. Install any prerequisites necessary.
1. Copy the file to the `modules` directory of your bot.

### Usage 

In your channel, or via message, enter `.bible some_verse`.  The API recognizes shortened versions of book names, e.g. Romans, Roma, and Rom are all the same.

### Examples

* .bible John 3:16
* .bible 1 Tim 1:3-8

### Forthcoming

None
